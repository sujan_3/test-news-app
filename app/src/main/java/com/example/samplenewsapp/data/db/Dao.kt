package com.example.samplenewsapp.data.db

import androidx.room.*
import com.example.samplenewsapp.data.AllNews
import com.example.samplenewsapp.data.TopHeadlines


/**
 * Created by Sujan Rai on 1/22/2022.
 * sujan.rai@spiralogics.com
 * Description:
 *
 */

@Dao
interface TopHeadlineNewsDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(users: List<TopHeadlines>)

    // For now, Room only supports PagingSource with Key of type Int.
    /*@Query("SELECT * FROM top_headlines")
    fun fetchTopHeadlines(): PagingSource<String, TopHeadlines>*/

    @Query("SELECT * FROM top_headlines ORDER BY epochTimeStamp DESC LIMIT :pageSize")
    fun fetchTopHeadlines(pageSize: Int): List<TopHeadlines>

    @Query("SELECT COUNT(title) FROM top_headlines")
    fun getCount(): Int

    @Query("DELETE FROM top_headlines")
    suspend fun clearAll()

    /**
     * Get title[TopHeadlines.title] (used as key since there's no id on payload from API) of the latest Top Headlines
     */
    @Query("SELECT title FROM top_headlines ORDER BY publishedAt ASC LIMIT 1")
    fun fetchPreviousKey(): String

}

@Dao
interface AllNewsDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(users: List<AllNews>)

    @Query("SELECT * FROM all_news WHERE epochTimeStamp < :nextKey ORDER BY epochTimeStamp DESC LIMIT :pageSize")
    fun fetchAllNews(pageSize: Int, nextKey: Long): List<AllNews>

    @Query("SELECT COUNT(title) FROM all_news")
    fun getCount(): Int

    /**
     * Get title[TopHeadlines.title] (used as key since there's no id on payload from API) of the latest Top Headlines
     */
    @Query("SELECT title FROM all_news ORDER BY publishedAt ASC LIMIT 1")
    fun fetchPreviousKey(): String

    @Query("SELECT epochTimeStamp FROM all_news ORDER BY epochTimeStamp DESC LIMIT 1")
    fun fetchLatestEmailTimestamp(): Long

}