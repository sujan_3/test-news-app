package com.example.samplenewsapp.data.topheadlines

import android.util.Log
import com.example.samplenewsapp.BuildConfig
import com.example.samplenewsapp.data.AllNews
import com.example.samplenewsapp.data.TopHeadlines
import com.example.samplenewsapp.data.network.Api
import com.example.samplenewsapp.data.network.ApiService
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Single


/**
 * Created by Sujan Rai on 1/23/2022.
 * sujan.rai@spiralogics.com
 * Description:
 *
 */
class NetworkTopHeadlinesRunner {
    private val apiService: ApiService = Api.apiInstance.create(ApiService::class.java)


    fun prefetchTopHeadlines(page: Int): Single<List<TopHeadlines>> = Single.create { emitter ->
        Log.d(TAG, "prefetchTopHeadlines: ")
        apiService.getTopHeadlines(
            "us",
            PASS_THROUGH_CACHE_SIZE,
            page,
            BuildConfig.API_KEY
        )
            .subscribe({ topHeadlineInfo ->

                Log.d(TAG, "prefetchTopHeadlines: onSuccess()")

                val topHeadlines = topHeadlineInfo.articles

                if (topHeadlines.isEmpty()) {
                    emitter.onError(Exception("All data fetched"))
                    return@subscribe
                }
                emitter.onSuccess(topHeadlines)
            }, { error ->
                Log.d(TAG, "prefetchTopHeadlines: onError() " + error.localizedMessage)

                emitter.onError(error)
            })
    }

    fun fetchTopHeadlines(page: Int, pageSize: Int): Single<List<TopHeadlines>> =
        Single.create { emitter ->
            Log.d(TAG, "fetchTopHeadlines: ")

            apiService.getTopHeadlines(
                "us",
                PAGE_SIZE,
                page,
                BuildConfig.API_KEY
            )
                .subscribe({ topHeadlineInfo ->
                    Log.d(TAG, "fetchTopHeadlines: onSuccess()")

                    emitter.onSuccess(topHeadlineInfo.articles)
                }, { error ->
                    Log.d(TAG, "fetchTopHeadlines: onError() " + error.localizedMessage)

                    emitter.onError(error)
                })
        }

    fun prefetchAllNews(page: Int): Observable<List<AllNews>> = Observable.create { emitter ->
        Log.d(TAG, "prefetchAllNews: " + page)
        apiService.getAllNews(
            "everything",
            PASS_THROUGH_CACHE_SIZE,
            page,
            BuildConfig.API_KEY
        )
            .subscribe({ topHeadlineInfo ->

                Log.d(TAG, "prefetchAllNews: onSuccess()")

                val topHeadlines = topHeadlineInfo.articles

                if (topHeadlines.isEmpty()) {
                    emitter.onError(Exception("All data fetched"))
                    return@subscribe
                }
                emitter.onNext(topHeadlines)
            }, { error ->
                Log.d(TAG, "prefetchAllNews: onError() " + error.localizedMessage)

                emitter.onError(error)
            })
    }


    fun fetchAllNews(query: String, page: Int): Observable<List<AllNews>> =
        Observable.create { emitter ->
            Log.d(TAG, "fetchAllNews: ")

            apiService.getAllNews(
                query,
                PAGE_SIZE,
                page,
                BuildConfig.API_KEY
            )
                .subscribe({ topHeadlineInfo ->
                    Log.d(TAG, "fetchAllNews: onSuccess()")

                    emitter.onNext(topHeadlineInfo.articles)
                }, { error ->
                    Log.d(TAG, "fetchAllNews: onError() " + error.localizedMessage)

                    emitter.onError(error)
                })
        }

    companion object {
        private const val TAG = "NetworkTopHeadlinesRunn"

        private const val PASS_THROUGH_CACHE_SIZE = 100

        private const val PAGE_SIZE = 20

    }
}