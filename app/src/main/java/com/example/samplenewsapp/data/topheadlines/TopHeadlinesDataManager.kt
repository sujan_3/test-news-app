package com.example.samplenewsapp.data.topheadlines

import android.content.Context
import android.util.Log
import com.example.samplenewsapp.data.AllNews
import com.example.samplenewsapp.data.TopHeadlines
import com.example.samplenewsapp.ui.MainActivity
import com.example.samplenewsapp.utcToEpoch
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.schedulers.Schedulers
import java.util.*


/**
 * Created by Sujan Rai on 1/23/2022.
 * sujan.rai@spiralogics.com
 * Description:
 *
 */
class TopHeadlinesDataManager(context: Context) {

    private var networkRunner = NetworkTopHeadlinesRunner()

    private var localRepositoryRunner = LocalRepositoryTopHeadlinesRuner(context)


    /**
     * Prefetch data from API and store in db
     * There is possibility of missing data if worker fails to fetch all data till latest stored in db
     * Need to purge or use other technique, which i haven't handled here
     */
    fun prefetchTopHeadlines(
        networkRunner: NetworkTopHeadlinesRunner,
        localRepositoryRunner: LocalRepositoryTopHeadlinesRuner
    ): Completable = Completable.create { emitter ->
        this.networkRunner = networkRunner
        this.localRepositoryRunner = localRepositoryRunner

        callNetworkRunnerToPrefetchTopHeadlines(
            FIRST_PAGE,
            localRepositoryRunner.fetchTopHeadlinesPreviousKey()
        )
    }

    private fun callNetworkRunnerToPrefetchTopHeadlines(page: Int, prevKey: String) {
        Log.d(TAG, "callNetworkRunnerToPrefetchTopHeadlines: " + page + " prevKey: " + prevKey)
        networkRunner.prefetchTopHeadlines(page)
            .subscribeOn(Schedulers.computation())
            .map(this::updateTopHeadlines)
            .subscribe({ topHeadlines ->
                Log.d(TAG, "callNetworkRunnerToPrefetchTopHeadlines: onSuccess()")

                localRepositoryRunner.storeTopHeadlinesLocally(topHeadlines)

                if ((topHeadlines.find { it.title.contentEquals(prevKey) }) != null) {
                    return@subscribe
                }

                callNetworkRunnerToPrefetchTopHeadlines(page + 1, prevKey)

            }, {
                Log.d(TAG, "callNetworkRunnerToPrefetchTopHeadlines: " + it.localizedMessage)
            })
    }


    fun prefetchAllNews(
        networkRunner: NetworkTopHeadlinesRunner,
        localRepositoryRunner: LocalRepositoryTopHeadlinesRuner
    ): Completable = Completable.create { emitter ->
        this.networkRunner = networkRunner
        this.localRepositoryRunner = localRepositoryRunner

        callNetworkRunnerToPrefetchAllNews(
            FIRST_PAGE,
            localRepositoryRunner.fetchAllNewsPreviousKey()
        )
    }

    private fun callNetworkRunnerToPrefetchAllNews(page: Int, prevKey: String) {
        Log.d(TAG, "callNetworkRunnerToPrefetchAllNews: " + page + " prevKey: " + prevKey)
        networkRunner.prefetchAllNews(page)
            .subscribeOn(Schedulers.computation())
            .map(this::updateAllNews)
            .subscribe({ topHeadlines ->
                Log.d(TAG, "callNetworkRunnerToPrefetchAllNews: onSuccess()")

                localRepositoryRunner.storeAllNewsLocally(topHeadlines)

                if ((topHeadlines.find { it.title.contentEquals(prevKey) }) != null) {
                    return@subscribe
                }

                callNetworkRunnerToPrefetchAllNews(page + 1, prevKey)

            }, {
                Log.d(TAG, "callNetworkRunnerToPrefetchAllNews: " + it.localizedMessage)
            })
    }

    private fun updateTopHeadlines(topHeadlines: List<TopHeadlines>): List<TopHeadlines> {
        topHeadlines.map { it.epochTimeStamp = it.publishedAt.utcToEpoch() }
        return topHeadlines
    }

    private fun updateAllNews(allNews: List<AllNews>): List<AllNews> {
        allNews.map { it.epochTimeStamp = it.publishedAt?.utcToEpoch()!! }
        return allNews
    }

    fun fetchTopHeadlines(
        nextKey: Long,
        page: Int,
        pageSize: Int
    ): Single<List<TopHeadlines>> = Single.create { emitter ->

        val topHeadlines = localRepositoryRunner.getTopHeadlines(nextKey, pageSize)
        if (!topHeadlines.isEmpty()) {
            emitter.onSuccess(topHeadlines)
            return@create
        }

        networkRunner.fetchTopHeadlines(page, pageSize)
            .subscribe({
                emitter.onSuccess(it)
            }, {
                emitter.onError(it)
            })

    }

    fun fetchAllNews(
        query: String,
        nextKey: Long,
        page: Int
    ): Observable<List<AllNews>> = Observable.create { emitter ->
        Log.d(TAG, "fetchAllNews: ")

        val allNews = localRepositoryRunner.getAllNews(nextKey)
        Log.d(TAG, "fetchAllNews: " + allNews.size)
        if (!allNews.isEmpty()) {
            emitter.onNext(allNews)
            return@create
        }

        // These network calls will possibly fetch data stored in db since current API doesn't take nextkey or prevKey for pagination
        networkRunner.fetchAllNews(query, page)
            .subscribe({
                emitter.onNext(it)
            }, {
                emitter.onError(it)
            })
    }

    fun fetchLatestEmailTimestamp(): Single<Long> = Single.create { emitter ->
        localRepositoryRunner.fetchLatestEmailTimestamp()
            .subscribe({
                emitter.onSuccess(it)
            }, {
                emitter.onError(it)
            })
    }

    companion object {
        private const val TAG = "TopHeadlinesDataManager"

        private const val FIRST_PAGE = 1
    }
}