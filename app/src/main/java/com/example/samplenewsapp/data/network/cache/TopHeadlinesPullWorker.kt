package com.example.samplenewsapp.data.network.cache

import android.content.Context
import android.util.Log
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.example.samplenewsapp.data.topheadlines.LocalRepositoryTopHeadlinesRuner
import com.example.samplenewsapp.data.topheadlines.NetworkTopHeadlinesRunner
import com.example.samplenewsapp.data.topheadlines.TopHeadlinesDataManager
import io.reactivex.rxjava3.schedulers.Schedulers

class TopHeadlinesPullWorker(appContext: Context, workerParams: WorkerParameters) :
    Worker(appContext, workerParams) {

    override fun doWork(): Result {
        Log.d(TAG, "doWork: ")

        TopHeadlinesDataManager(applicationContext).prefetchTopHeadlines(
            NetworkTopHeadlinesRunner(),
            LocalRepositoryTopHeadlinesRuner(applicationContext)
        ).subscribeOn(Schedulers.trampoline())
            .observeOn(Schedulers.trampoline())
            .subscribe({

            }, {

            })

        // Indicate whether the work finished successfully with the Result
        return Result.success()
    }

    companion object {
        private const val TAG = "TopHeadlinesPullWorker"
    }
}
