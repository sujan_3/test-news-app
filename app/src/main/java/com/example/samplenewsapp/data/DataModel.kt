package com.example.samplenewsapp.data

import androidx.annotation.Nullable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters

/**
 * Created by Sujan Rai on 1/22/2022.
 * sujan.rai@spiralogics.com
 * Description:
 *
 */


data class NewsSource(
    val id: String,
    val name: String
)

@Entity(tableName = "top_headlines")
data class TopHeadlines(
    @PrimaryKey(autoGenerate = true)
    val pk: Int,
    @TypeConverters(NewsSourceConverter::class)
    val source: NewsSource,
    @Nullable
    @ColumnInfo(defaultValue = "")
    val author: String?,
    val title: String,
    @Nullable
    @ColumnInfo(defaultValue = "")
    val description: String,
    val url: String,
    @Nullable
    @ColumnInfo(defaultValue = "")
    val urlToImage: String,
    var publishedAt: String,
    var epochTimeStamp: Long,
    @Nullable
    @ColumnInfo(defaultValue = "")
    val content: String
)

data class TopHeadingsInfo(
    val status: String,
    val totalResults: Int,
    val articles: List<TopHeadlines>
)

@Entity(tableName = "all_news")
data class AllNews(
    @TypeConverters(NewsSourceConverter::class)
    val source: NewsSource?,
    @Nullable
    @ColumnInfo(defaultValue = "")
    val author: String?,
    val title: String,
    @Nullable
    @ColumnInfo(defaultValue = "")
    val description: String?,
    @Nullable
    @ColumnInfo(defaultValue = "")
    val url: String?,
    @Nullable
    @ColumnInfo(defaultValue = "")
    val urlToImage: String?,
    @Nullable
    @ColumnInfo(defaultValue = "")
    var publishedAt: String?,
    @PrimaryKey
    var epochTimeStamp: Long,
    @Nullable
    @ColumnInfo(defaultValue = "")
    val content: String?
) {
    constructor(title: String) : this(null, null, title, null, null, null, null, 0L, null)
}

data class AllNewsInfo(
    val status: String,
    val totalResults: Int,
    val articles: List<AllNews>
)

