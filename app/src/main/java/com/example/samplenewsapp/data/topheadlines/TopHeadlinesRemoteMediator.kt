package com.example.samplenewsapp.data.topheadlines

import android.util.Log
import androidx.paging.ExperimentalPagingApi
import androidx.paging.LoadType
import androidx.paging.PagingState
import androidx.paging.RemoteMediator
import androidx.room.withTransaction
import com.example.samplenewsapp.data.TopHeadlines
import com.example.samplenewsapp.data.db.AppDb
import com.example.samplenewsapp.data.network.ApiService
import retrofit2.HttpException
import java.io.IOException
import java.lang.Exception


/**
 * Created by Sujan Rai on 1/22/2022.
 * sujan.rai@spiralogics.com
 * Description:
 *
 */

@OptIn(ExperimentalPagingApi::class)
class TopHeadlinesRemoteMediator(
    private val query: String,
    private val database: AppDb,
    private val networkService: ApiService
) : RemoteMediator<String, TopHeadlines>() {

    val topHeadlinesDao = database.topHeadlinesNewsDao()

    override suspend fun load(
        loadType: LoadType,
        state: PagingState<String, TopHeadlines>
    ): MediatorResult {
        Log.d(TAG, "load: ")

        return try {

            val loadKey = when (loadType) {
                LoadType.REFRESH -> null

                LoadType.PREPEND ->
                    return MediatorResult.Success(endOfPaginationReached = true)

                LoadType.APPEND -> {
                    val lastItem = state.lastItemOrNull()

                    if (lastItem == null) {
                        return MediatorResult.Success(
                            endOfPaginationReached = true
                        )
                    }
                    lastItem.publishedAt
                }
            }

            /*val response = networkService.getTopHeadlines(
                "en", 20, 0, ""
            )

            database.withTransaction {
                if (loadType == LoadType.REFRESH) {
                    // topHeadlinesDao.deleteByQuery(query)
                }

                topHeadlinesDao.insertAll(response.value!!.articles)
            }

            MediatorResult.Success(
                endOfPaginationReached = response.value?.totalResults == 0
            )*/

            MediatorResult.Error(Exception())

        } catch (e: IOException) {
            MediatorResult.Error(e)
        } catch (e: HttpException) {
            MediatorResult.Error(e)
        }

    }

    companion object {
        private const val TAG = "TopHeadlinesRemoteMedia"
    }

}