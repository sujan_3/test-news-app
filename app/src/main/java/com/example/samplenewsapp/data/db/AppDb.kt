package com.example.samplenewsapp.data.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.example.samplenewsapp.data.AllNews
import com.example.samplenewsapp.data.NewsSourceConverter
import com.example.samplenewsapp.data.TopHeadlines
import dagger.Provides

@Database(entities = [TopHeadlines::class, AllNews::class], version = 1)
@TypeConverters(NewsSourceConverter::class)
abstract class AppDb : RoomDatabase() {

    abstract fun topHeadlinesNewsDao(): TopHeadlineNewsDao

    abstract fun allNewsDao(): AllNewsDao
}
