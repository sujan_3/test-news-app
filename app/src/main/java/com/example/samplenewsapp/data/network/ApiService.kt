package com.example.samplenewsapp.data.network

import com.example.samplenewsapp.data.AllNewsInfo
import com.example.samplenewsapp.data.TopHeadingsInfo
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {

    @GET("/v2/top-headlines")
    fun getTopHeadlines(
        @Query("country") country: String,
        @Query("pageSize") pageSize: Int,
        @Query("page") page: Int,
        @Query("apiKey") apiKey: String
    ): Single<TopHeadingsInfo>

    @GET("/v2/everything")
    fun getAllNews(
        @Query("q") query: String,
        @Query("pageSize") pageSize: Int,
        @Query("page") page: Int,
        @Query("apiKey") apiKey: String
    ): Observable<AllNewsInfo>
}
