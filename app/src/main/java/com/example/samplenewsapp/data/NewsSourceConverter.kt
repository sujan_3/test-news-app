package com.example.samplenewsapp.data

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken


/**
 * Created by Sujan Rai on 1/23/2022.
 * sujan.rai@spiralogics.com
 * Description:
 *
 */

class NewsSourceConverter {

    @TypeConverter
    fun toObject(data: String): NewsSource {
        val objectType = object : TypeToken<NewsSource>() {}.type
        return Gson().fromJson(data, objectType)
    }

    @TypeConverter
    fun toString(newsSource: NewsSource): String {
        return Gson().toJson(newsSource)
    }
}