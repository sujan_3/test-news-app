package com.example.samplenewsapp.data.network.cache

import android.content.Context
import androidx.work.*
import com.example.samplenewsapp.data.network.cache.TopHeadlinesPullWorker
import com.example.samplenewsapp.data.topheadlines.LocalRepositoryTopHeadlinesRuner
import com.example.samplenewsapp.data.topheadlines.NetworkTopHeadlinesRunner
import com.example.samplenewsapp.data.topheadlines.TopHeadlinesDataManager
import io.reactivex.rxjava3.schedulers.Schedulers
import java.util.concurrent.TimeUnit


/**
 * Created by Sujan Rai on 1/23/2022.
 * sujan.rai@spiralogics.com
 * Description:
 *
 */
class AllNewsPullWorker(appContext: Context, workerParams: WorkerParameters) :
    Worker(appContext, workerParams) {

    override fun doWork(): Result {

        TopHeadlinesDataManager(applicationContext).prefetchAllNews(
            NetworkTopHeadlinesRunner(),
            LocalRepositoryTopHeadlinesRuner(applicationContext)
        ).subscribeOn(Schedulers.trampoline())
            .observeOn(Schedulers.trampoline())
            .subscribe({

            }, {

            })


        // Indicate whether the work finished successfully with the Result
        return Result.success()
    }
}
