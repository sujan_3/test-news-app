package com.example.samplenewsapp.data.topheadlines

import android.content.Context
import android.util.Log
import com.example.samplenewsapp.Constants.PAGE_SIZE
import com.example.samplenewsapp.data.AllNews
import com.example.samplenewsapp.data.TopHeadlines
import com.example.samplenewsapp.data.db.AllNewsDao
import com.example.samplenewsapp.data.db.TopHeadlineNewsDao
import dagger.hilt.EntryPoint
import dagger.hilt.InstallIn
import dagger.hilt.android.EntryPointAccessors
import dagger.hilt.components.SingletonComponent
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.schedulers.Schedulers
import java.lang.IndexOutOfBoundsException


/**
 * Created by Sujan Rai on 1/23/2022.
 * sujan.rai@spiralogics.com
 * Description:
 *
 */
class LocalRepositoryTopHeadlinesRuner(context: Context) {

    @InstallIn(SingletonComponent::class)
    @EntryPoint
    interface RepositoryEntryPoint {
        fun topHeadlinesDao(): TopHeadlineNewsDao

        fun allNewsDao(): AllNewsDao
    }

    fun getTopHeadlinesDao(context: Context): TopHeadlineNewsDao {
        val hiltEntryPoint = EntryPointAccessors.fromApplication(
            context,
            RepositoryEntryPoint::class.java
        )

        return hiltEntryPoint.topHeadlinesDao()
    }

    var topHeadlinesDao: TopHeadlineNewsDao = getTopHeadlinesDao(context)

    fun getAllNewsDao(context: Context): AllNewsDao {
        val hiltEntryPoint = EntryPointAccessors.fromApplication(
            context,
            RepositoryEntryPoint::class.java
        )

        return hiltEntryPoint.allNewsDao()
    }

    var allNewsDao: AllNewsDao = getAllNewsDao(context)

    fun hasData(): Boolean {
        return topHeadlinesDao.getCount() > 0
    }

    fun fetchTopHeadlinesPreviousKey(): String {
        if (hasData()) {
            val prevKey = topHeadlinesDao.fetchPreviousKey()
            return prevKey
        }

        return ""
    }

    fun storeTopHeadlinesLocally(topHeadlines: List<TopHeadlines>) {

        Observable.fromIterable(topHeadlines).subscribeOn(Schedulers.io())
            .buffer(99)
            .subscribe({
                topHeadlinesDao.insertAll(it)
            }, {
                Log.e(TAG, "storeTopHeadlinesLocally: " + it.localizedMessage)
            })
    }

    fun fetchAllNewsPreviousKey(): String {
        if (hasData()) {
            val prevKey = topHeadlinesDao.fetchPreviousKey()
            return prevKey
        }

        return ""
    }

    fun storeAllNewsLocally(allNews: List<AllNews>) {
        Observable.fromIterable(allNews).subscribeOn(Schedulers.io())
            .buffer(99)
            .subscribe({
                allNewsDao.insertAll(it)
            }, {
                Log.e(TAG, "storeAllNewsLocally: " + it.localizedMessage)
            })
    }

    fun getTopHeadlines(nextKey: Long, pageSize: Int): List<TopHeadlines> {
        return topHeadlinesDao.fetchTopHeadlines(pageSize)
    }

    fun getAllNews(nextKey: Long): List<AllNews> {
        Log.d(TAG, "getAllNews: " + nextKey)
        return allNewsDao.fetchAllNews(PAGE_SIZE, nextKey)
    }

    fun hasAllNews(): Boolean {
        return allNewsDao.getCount() > 0
    }

    fun fetchLatestEmailTimestamp(): Single<Long> = Single.create { emitter ->
        if(hasAllNews()) {
            emitter.onSuccess(allNewsDao.fetchLatestEmailTimestamp())
            return@create
        }

        emitter.onError(IndexOutOfBoundsException())
    }

    companion object {
        private const val TAG = "LocalRepositoryTopHeadl"
    }
}