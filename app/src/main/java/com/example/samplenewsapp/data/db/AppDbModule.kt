package com.example.samplenewsapp.data.db

import android.content.Context
import androidx.room.Room
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent


/**
 * Created by Sujan Rai on 1/23/2022.
 * sujan.rai@spiralogics.com
 * Description:
 *
 */

@InstallIn(SingletonComponent::class)
@Module
class AppDbModule {

    @Provides
    fun providesTopHeadlinesNewsDao(db: AppDb): TopHeadlineNewsDao {
        return db.topHeadlinesNewsDao()
    }

     @Provides
    fun providesAllNewsDao(db: AppDb): AllNewsDao {
        return db.allNewsDao()
    }

    @Provides
    fun providesAppDatabase(@ApplicationContext context: Context): AppDb {
        return Room.databaseBuilder(context, AppDb::class.java, "app_db")
            .build()
    }

}