package com.example.samplenewsapp

import android.content.Context
import android.text.format.DateUtils
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.google.type.DateTime
import dagger.hilt.android.qualifiers.ApplicationContext
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.*


/**
 * Created by Sujan Rai on 1/23/2022.
 * sujan.rai@spiralogics.com
 * Description:
 *
 */

// Recyclerview and Adapter
fun <T> RecyclerView.Adapter<*>.autoNotify(
    oldList: MutableList<T>,
    newList: MutableList<T>,
    compare: (T, T) -> Boolean
) {
    val diff = DiffUtil.calculateDiff(object : DiffUtil.Callback() {
        override fun getOldListSize() = oldList.size

        override fun getNewListSize() = newList.size

        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return compare(oldList[oldItemPosition], newList[newItemPosition])
        }

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return oldList[oldItemPosition] == newList[newItemPosition]
        }
    })

    diff.dispatchUpdatesTo(this)
}

fun String.utcToEpoch(): Long {
    val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")
    simpleDateFormat.timeZone = TimeZone.getTimeZone("UTC")
    val myDate: Date = simpleDateFormat.parse(this)
    return myDate.time
}




