package com.example.samplenewsapp.ui.adapter

import android.content.Context
import android.text.format.DateUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.samplenewsapp.BaseViewHolder
import com.example.samplenewsapp.data.AllNews
import com.example.samplenewsapp.databinding.RowAllMailDescriptiveBinding
import com.example.samplenewsapp.databinding.RowAllMailNormalBinding
import com.example.samplenewsapp.databinding.RowFooterBinding
import com.example.samplenewsapp.ui.AllNewsClickListener

class AllNewsAdapter constructor(
    val context: Context,
    val allNews: ArrayList<AllNews>,
    val listener: AllNewsClickListener,
) : RecyclerView.Adapter<BaseViewHolder<*>>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<*> {
        return when (viewType) {
            VIEW_TYPE_REGULAR -> RegularViewHolder(
                RowAllMailNormalBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            )

            VIEW_TYPE_DESCRIPTIVE -> DescriptiveViewHolder(
                RowAllMailDescriptiveBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            )
            VIEW_TYPE_FOOTER -> FooterViewHolder(
                RowFooterBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            )

            else -> throw IllegalArgumentException("Wrong view type")
        }
    }

    override fun onBindViewHolder(holder: BaseViewHolder<*>, position: Int) {
        val news = allNews.get(position)
        when (holder) {
            is RegularViewHolder -> holder.bind(news)
            is DescriptiveViewHolder -> holder.bind(news)
            is FooterViewHolder -> holder.bind(news)
            else -> throw IllegalArgumentException("Wrong view type")
        }
    }

    override fun getItemCount(): Int = allNews.size

    override fun getItemViewType(position: Int): Int {
        val o = allNews[position]
        Log.d(TAG, "getItemViewType: " + o.title)
        if (o.title.equals(FOOTER))
            return VIEW_TYPE_FOOTER
        else
            return if (position % 5 == 0) VIEW_TYPE_DESCRIPTIVE else VIEW_TYPE_REGULAR
    }

    fun updateAllNews(mAllNews: List<AllNews>) {
        Log.d(TAG, "updateAllNews: " + mAllNews.size)

        val startPosition = itemCount - 1

        allNews.addAll(mAllNews)

        notifyItemRangeChanged(startPosition, itemCount - 1)
    }

    fun addFooter() {
        Log.d(TAG, "addFooter: ")
        allNews.add(AllNews(FOOTER))
        notifyDataSetChanged()
    }

    fun removeFooter() {
        try {
            allNews.removeAt(allNews.size - 1)
            notifyDataSetChanged()
        } catch (e: IndexOutOfBoundsException) {
        }

    }

    inner class RegularViewHolder(val binding: RowAllMailNormalBinding) :
        BaseViewHolder<AllNews>(binding.root) {

        override fun bind(news: AllNews) {
            binding.sourceTv.text = news.source?.name
            binding.titleTv.text = news.title
            binding.contentTv.text = news.content
            binding.publishedAtTv.text =
                DateUtils.getRelativeTimeSpanString(news.epochTimeStamp)

            binding.root.setOnClickListener {
                listener.onAllNewsclicked(news)
            }
        }

    }

    inner class DescriptiveViewHolder(val binding: RowAllMailDescriptiveBinding) :
        BaseViewHolder<AllNews>(binding.root) {

        override fun bind(news: AllNews) {
            binding.sourceTv.text = news.source?.name
            binding.titleTv.text = news.title
            binding.contentTv.text = news.content
            binding.publishedAtTv.text =
                DateUtils.getRelativeTimeSpanString(news.epochTimeStamp)
            Glide.with(context).load(news.urlToImage).into(binding.newsIv)


            binding.root.setOnClickListener {
                listener.onAllNewsclicked(news)
            }
        }

    }

    inner class FooterViewHolder(binding: RowFooterBinding) :
        BaseViewHolder<AllNews>(binding.root) {

        override fun bind(news: AllNews) {
        }

    }

    companion object {
        private val TAG = AllNewsAdapter::class.java.simpleName

        private const val VIEW_TYPE_REGULAR = 0
        private const val VIEW_TYPE_DESCRIPTIVE = 1
        private const val VIEW_TYPE_FOOTER = 2

        private const val FOOTER = "footer"
    }


}