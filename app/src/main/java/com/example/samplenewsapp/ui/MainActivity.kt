package com.example.samplenewsapp.ui

import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.browser.customtabs.CustomTabsIntent
import androidx.core.view.WindowCompat
import androidx.core.widget.NestedScrollView
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.work.ExistingPeriodicWorkPolicy
import androidx.work.PeriodicWorkRequestBuilder
import androidx.work.WorkManager
import com.example.samplenewsapp.data.AllNews
import com.example.samplenewsapp.data.TopHeadlines
import com.example.samplenewsapp.data.network.cache.AllNewsPullWorker
import com.example.samplenewsapp.data.network.cache.TopHeadlinesPullWorker
import com.example.samplenewsapp.data.topheadlines.TopHeadlinesDataManager
import com.example.samplenewsapp.databinding.ActivityMainBinding
import com.example.samplenewsapp.ui.adapter.AllNewsAdapter
import com.example.samplenewsapp.ui.adapter.MainViewModel
import com.example.samplenewsapp.ui.adapter.TopHeadlinesAdapter
import com.example.samplenewsapp.ui.factory.MainViewModelFactory
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.schedulers.Schedulers
import java.util.concurrent.TimeUnit

/**
 * Created by Sujan Rai on 1/23/2022.
 *
 * Description: Activity to display All news and Top 5 headlines
 * App db (sqlite) will always be the source of truth for data
 * Worker with periodic jobs fetch data from server continuously
 * Direct api calls to fetch data is only done when app db has no more data to serve
 *
 * Limitations: Latest data from worker stored in db after the point when app db has started serving data
 * isn't fetched in UI. Task to observe it is pending.
 */
class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    private lateinit var model: MainViewModel

    private lateinit var allMailsLayoutManager: LinearLayoutManager

    private lateinit var topHeadlinesAdapter: TopHeadlinesAdapter

    private lateinit var topAllNewsAdapter: AllNewsAdapter
    private lateinit var bottomAllNewsAdapter: AllNewsAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        WindowCompat.setDecorFitsSystemWindows(window, false)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        model = ViewModelProvider(
            this, MainViewModelFactory(
                TopHeadlinesDataManager(this@MainActivity)
            )
        ).get(MainViewModel::class.java)

        prepareViews()

        subscribe()

        model.fetchLatestEmailTimestamp("everything")

        fetchTop5Headlines()

        runPassThroughCache()

        model.shouldAddFooter().observe(this, this::addFooter)
    }

    private fun addFooter(value: Boolean) {
        bottomAllNewsAdapter.addFooter()
    }

    private fun subscribe() {
        model.createAllNewsSubject()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe({

                it.forEach { Log.d(TAG, "subscribe: " + it.title + " : " + it.epochTimeStamp) }
                Log.d(TAG, "subscribe: " + it.size)

                updateAllNews(it)
            }, {
                Log.e(TAG, "subscribe: " + it.localizedMessage)
            })

        model.createFooterSubject()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.computation())
            .subscribe({
                Log.d(TAG, "createFooterSubject() subscribe: onSuccess")

                bottomAllNewsAdapter.addFooter()

            }, {
                Log.e(TAG, "createFooterSubject() subscribe: onError()", )
            })
    }

    private fun fetchTop5Headlines() {
        model.fetchLatestFiveTopHeadlines()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe({
                Log.d(TAG, "fetchTop5Headlines: " + it.size)

                updateTopHeadlines(it)
            }, {
                Log.e(TAG, "fetchTop5Headlines: " + it.localizedMessage)
            })
    }

    private fun fetchAllNews(query: String) {
        Log.d(TAG, "fetchAllNews: ")
        model.fetchAllNews(query)
    }

    private fun updateAllNews(it: List<AllNews>) {
        if (topAllNewsAdapter.itemCount == EMPTY_TOP_LIST) {
            topAllNewsAdapter.updateAllNews(it.subList(0, 5))
            bottomAllNewsAdapter.updateAllNews(it.subList(5, it.size))
            return
        }

        bottomAllNewsAdapter.removeFooter()
        bottomAllNewsAdapter.updateAllNews(ArrayList(it))
    }

    private fun updateTopHeadlines(topHeadlines: List<TopHeadlines>) {
        topHeadlinesAdapter.topHeadlines = topHeadlines.toMutableList()
    }

    private fun prepareViews() {
        // Views for All News
        // views for top rv
        binding.topRv.layoutManager =
            LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        //setting adapter
        topAllNewsAdapter = AllNewsAdapter(
            this@MainActivity,
            ArrayList(0),
            allNewsClickListener
        )
        binding.topRv.adapter = topAllNewsAdapter

        // views for bottom rv
        allMailsLayoutManager = LinearLayoutManager(this@MainActivity, RecyclerView.VERTICAL, false)
        binding.bottomRv.setLayoutManager(allMailsLayoutManager)
        //setting adapter
        bottomAllNewsAdapter = AllNewsAdapter(
            this@MainActivity,
            ArrayList(0),
            allNewsClickListener
        )
        binding.bottomRv.adapter = bottomAllNewsAdapter
        binding.container.setOnScrollChangeListener(allMailsScrollListener)

        // Views for Top Headlines
        binding.topHeadlinesRv.layoutManager =
            LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        binding.topHeadlinesRv.itemAnimator = DefaultItemAnimator()
        //setting adapter
        topHeadlinesAdapter = TopHeadlinesAdapter(
            topHeadlinesClickListener
        )
        binding.topHeadlinesRv.adapter = topHeadlinesAdapter
    }


    /**
     * These are the scheduled jobs meant to sync data from server with local app db
     */
    private fun runPassThroughCache() {
        Log.d(TAG, "runPassThroughCache: ")

        val pullTopHeadlines =
            PeriodicWorkRequestBuilder<TopHeadlinesPullWorker>(6, TimeUnit.HOURS)
                .build()
        WorkManager.getInstance(this)
            .enqueueUniquePeriodicWork(
                "pullTopHeadlines",
                ExistingPeriodicWorkPolicy.REPLACE,
                pullTopHeadlines
            )

        val allNews =
            PeriodicWorkRequestBuilder<AllNewsPullWorker>(6, TimeUnit.HOURS)
                .build()
        WorkManager.getInstance(this)
            .enqueueUniquePeriodicWork(
                "pullAllNews",
                ExistingPeriodicWorkPolicy.REPLACE,
                allNews
            )

    }


    val topHeadlinesClickListener = object : TopHeadlinesClickListener {
        override fun onTopHeadlineClicked(topHeadline: TopHeadlines) {
            Log.d(TAG, "onTopHeadlineClicked: " + topHeadline)

            open(topHeadline.url)
        }
    }

    val allNewsClickListener = object : AllNewsClickListener {
        override fun onAllNewsclicked(allNews: AllNews) {
            Log.d(TAG, "onAllNewsclicked: ")

            open(allNews.url)
        }
    }

    private fun open(url: String?) {
        val builder = CustomTabsIntent.Builder()
        val customTabsIntent = builder.build()
        customTabsIntent.launchUrl(this@MainActivity, Uri.parse(url))
    }

    private val allMailsScrollListener: NestedScrollView.OnScrollChangeListener =
        object : NestedScrollView.OnScrollChangeListener {
            override fun onScrollChange(
                v: NestedScrollView,
                scrollX: Int,
                scrollY: Int,
                oldScrollX: Int,
                oldScrollY: Int
            ) {
                if (v.getChildAt(v.getChildCount() - 1) != null) {
                    if ((scrollY >= (v.getChildAt(v.getChildCount() - 1)
                            .getMeasuredHeight() - v.getMeasuredHeight())) &&
                        scrollY > oldScrollY
                    ) {
                        //code to fetch more data for endless scrolling
                        loadMoreItems()
                    }
                }
            }
        }

    /* private val allMailsScrollListener: NestedScrollView.OnScrollChangeListener =

         object : RecyclerView.OnScrollListener() {

             override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                 super.onScrolled(recyclerView, dx, dy)

                 Log.i(TAG, "onScrolled: ")

                 val visibleItemCount: Int = allMailsLayoutManager.getChildCount()
                 val totalItemCount: Int = allMailsLayoutManager.getItemCount()
                 val firstVisibleItemPosition: Int =
                     allMailsLayoutManager.findFirstVisibleItemPosition()

                 checkIfToLazyLoadAllNews(visibleItemCount, totalItemCount, firstVisibleItemPosition)
             }
         }*/

    private fun checkIfToLazyLoadAllNews(
        visibleItemCount: Int,
        totalItemCount: Int,
        firstVisibleItemPosition: Int
    ) {
        model.checkIftoLazyLoadAllNews(visibleItemCount, totalItemCount, firstVisibleItemPosition)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.computation())
            .subscribe({
                Log.d(TAG, "checkIfToLazyLoadAllNews: success")

                // addFooter()

                loadMoreItems()
            }, {
                Log.e(TAG, "checkIfToLazyLoadAllNews: " + it.localizedMessage)

                // not yet ready to lazy load
            })
    }

    private fun loadMoreItems() {
        fetchAllNews("everything")
    }


    companion object {
        private const val TAG = "MainActivity"

        private const val EMPTY_TOP_LIST = 0
    }
}