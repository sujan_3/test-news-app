package com.example.samplenewsapp.ui.adapter

import android.content.Context
import android.text.format.DateUtils
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.samplenewsapp.BaseViewHolder
import com.example.samplenewsapp.autoNotify
import com.example.samplenewsapp.data.TopHeadlines
import com.example.samplenewsapp.databinding.RowTopHeadlinesBinding
import com.example.samplenewsapp.ui.TopHeadlinesClickListener
import kotlin.properties.Delegates

class TopHeadlinesAdapter constructor(
    val listener: TopHeadlinesClickListener,
) : RecyclerView.Adapter<BaseViewHolder<*>>() {

    var topHeadlines: MutableList<TopHeadlines> by Delegates.observable(mutableListOf()) { property, oldList, newList ->
        autoNotify(oldList, newList) { o, n -> o.title == n.title }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<*> {
        return ViewHolder(
            RowTopHeadlinesBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: BaseViewHolder<*>, position: Int) {
        val onboardingCard = topHeadlines.get(position)
        when (holder) {
            is ViewHolder -> holder.bind(onboardingCard)
            else -> throw IllegalArgumentException()
        }
    }

    override fun getItemCount(): Int = topHeadlines.size

    inner class ViewHolder(val binding: RowTopHeadlinesBinding) :
        BaseViewHolder<TopHeadlines>(binding.root) {

        override fun bind(headline: TopHeadlines) {
            binding.sourceTv.text = headline.source.name

            binding.titleTv.text = headline.title

            binding.publishedAtTv.text =
                DateUtils.getRelativeTimeSpanString(headline.epochTimeStamp)

            binding.root.setOnClickListener {
                listener.onTopHeadlineClicked(headline)
            }
        }

    }

    companion object {
        private val TAG = TopHeadlinesAdapter::class.java.simpleName
    }


}