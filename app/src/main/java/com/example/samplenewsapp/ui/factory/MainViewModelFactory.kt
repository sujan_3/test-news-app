package com.example.samplenewsapp.ui.factory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.samplenewsapp.data.topheadlines.TopHeadlinesDataManager
import com.example.samplenewsapp.ui.adapter.MainViewModel

class MainViewModelFactory(private val dataManager: TopHeadlinesDataManager) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return MainViewModel(
            dataManager
        ) as T
    }

}
