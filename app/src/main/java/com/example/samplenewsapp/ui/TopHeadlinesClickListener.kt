package com.example.samplenewsapp.ui

import com.example.samplenewsapp.data.TopHeadlines


/**
 * Created by Sujan Rai on 1/23/2022.
 * sujan.rai@spiralogics.com
 * Description:
 *
 */
interface TopHeadlinesClickListener {

    fun onTopHeadlineClicked(topHeadline: TopHeadlines)
}