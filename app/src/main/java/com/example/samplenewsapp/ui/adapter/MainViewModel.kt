package com.example.samplenewsapp.ui.adapter

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.samplenewsapp.Constants
import com.example.samplenewsapp.Constants.PAGE_SIZE
import com.example.samplenewsapp.data.AllNews
import com.example.samplenewsapp.data.TopHeadlines
import com.example.samplenewsapp.data.topheadlines.TopHeadlinesDataManager
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Scheduler
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.schedulers.Schedulers
import io.reactivex.rxjava3.subjects.PublishSubject


/**
 * Created by Sujan Rai on 1/23/2022.
 * sujan.rai@spiralogics.com
 * Description:
 *
 */
class MainViewModel(private val topHeadlinesDataManager: TopHeadlinesDataManager) : ViewModel() {

    fun fetchLatestFiveTopHeadlines(): Single<List<TopHeadlines>> {
        return topHeadlinesDataManager.fetchTopHeadlines(
            0,
            1,
            PAGE_SIZE_FOR_DASHBOARD,
        )
    }

    private var isLoading = false
    private var isLastPage = false

    private var nextKey = Constants.DEFAULT_KEY
    private var page = Constants.FIRST_PAGE

    private lateinit var allNewsSubject: PublishSubject<List<AllNews>>

    fun createAllNewsSubject(): PublishSubject<List<AllNews>> {
        allNewsSubject = PublishSubject.create()
        return allNewsSubject
    }


    private lateinit var footerSubject: PublishSubject<Boolean>

    fun createFooterSubject(): PublishSubject<Boolean> {
        footerSubject = PublishSubject.create()
        return footerSubject
    }

    fun fetchAllNews(
        query: String,
    ) {

        Log.d(
            TAG,
            "fetchAllNews: " + isLoading + " nextKey: " + nextKey + " isLoading: " + isLoading
        )

        if (!isLoading && !isLastPage)
            topHeadlinesDataManager.fetchAllNews(
                query, nextKey, page
            ).subscribeOn(Schedulers.io())
                .doOnSubscribe({
                    this.isLoading = true
                    footerSubject.onNext(true)
                })
                .doOnNext({
                    this.nextKey = it.last().epochTimeStamp
                    this.isLoading = false
                    this.isLastPage = it.size < PAGE_SIZE
                })
                .doOnError { this.isLoading = false }
                .subscribe(allNewsSubject)
        else
            Log.d(
                TAG,
                "fetchAllNews: Either all data fetched or is already loading data -> isLoading: " + isLoading + " isLastPage: " + isLastPage
            )
    }


    private val toAddFooter = MutableLiveData<Boolean>()
    private fun addFooter() {
        toAddFooter.value = true
    }

    fun shouldAddFooter(): MutableLiveData<Boolean> {
        return toAddFooter
    }

    fun fetchLatestEmailTimestamp(query: String) {
        topHeadlinesDataManager.fetchLatestEmailTimestamp()
            .subscribeOn(Schedulers.io())
            .subscribe({
                Log.d(TAG, "fetchLatestEmailTimestamp: " + it)

                nextKey = it

                fetchAllNews(query)
            }, {
                Log.d(TAG, "fetchLatestEmailTimestamp: " + it.localizedMessage)

                fetchAllNews(query)
            })
    }

    fun checkIftoLazyLoadAllNews(
        visibleItemCount: Int,
        totalItemCount: Int,
        firstVisibleItemPosition: Int
    ): Completable = Completable.create { emitter ->
        if (!isLoading && !isLastPage) {
            if (visibleItemCount + firstVisibleItemPosition >= totalItemCount - Constants.PAGE_SIZE / 2 && firstVisibleItemPosition >= 0 && totalItemCount >= Constants.PAGE_SIZE - Constants.PAGE_SIZE / 2) {
                emitter.onComplete()
                return@create
            }

            throw Exception()

        }
        throw Exception()

    }


    companion object {
        private const val TAG = "MainViewModel"

        private const val PAGE_SIZE_FOR_DASHBOARD = 5
    }

}