package com.example.samplenewsapp

import android.app.Application
import dagger.Provides
import dagger.hilt.android.HiltAndroidApp
import dagger.hilt.android.qualifiers.ApplicationContext


/**
 * Created by Sujan Rai on 1/23/2022.
 * sujan.rai@spiralogics.com
 * Description:
 *
 */

@HiltAndroidApp
class App : Application() {

    override fun onCreate() {
        super.onCreate()
    }


}