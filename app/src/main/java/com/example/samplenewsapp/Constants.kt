package com.example.samplenewsapp


/**
 * Created by Sujan Rai on 1/24/2022.
 * sujan.rai@spiralogics.com
 * Description:
 *
 */
object Constants {

    const val FIRST_PAGE = 0
    const val DEFAULT_KEY = 0L
    const val PAGE_SIZE = 20
}